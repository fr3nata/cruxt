use std::env;
use std::fs;
use std::io::{BufReader, BufWriter};

extern crate tcx;

fn parse(file_path: &String) -> Result<tcx::TrainingCenterDatabase, String> {
    let file = fs::File::open(file_path).expect(&format!("Cannot open file {}", file_path));
    let mut reader = BufReader::new(file);
    let result = tcx::read(&mut reader);
    match result {
        Ok(db) => {
            println!("Calories burned: {}", db.activities.activity[0].lap.calories);
            return Ok(db);
        },
        Err(e) => {
            println!("{}", e);
            // TODO: panic or exit?
            return Err(e);
        }
    }
}

struct MergedLap {
    start_time: String,
    total_time: f32,
    calories: u32,
    cadence: u32,
    distance_meters: f32,
    maximum_speed: f32,
    intensity: String,
    trigger: String,
}

fn aggregate(acts: Vec<tcx::Activity>) -> MergedLap {
    let start = acts
        .iter()
        .fold("".to_string(), |acc, x| {
            if acc.is_empty() {
                x.lap.start_time.clone()
            } else {
                x.lap.start_time.clone().min(acc)
            }
        });

    let total_time = acts
        .iter()
        .fold(0.0, |acc, x| acc + x.lap.total_time_seconds);

    let calories = acts
        .iter()
        .fold(0, |acc, x| acc + x.lap.calories);

    let cadence = acts
        .iter()
        .fold(0, |acc, x| x.lap.cadence); // TODO: how to aggregate?

    let distance = acts
        .iter()
        .fold(0.0, |acc, x| acc + x.lap.distance_meters);

    let speed = acts
        .iter()
        .fold(0.0, |acc: f32, x| acc.max(x.lap.maximum_speed));

    return MergedLap {
        start_time: start,
        total_time: total_time,
        calories: calories,
        cadence: cadence,
        distance_meters: distance,
        maximum_speed: speed,
        intensity: "Active".to_string(), // TODO don't hardcode
        trigger: "Manual".to_string(),   // TODO don't hardcode
    }
}


#[derive(Debug, Default)]
struct StripConf {
    watts: bool,
    cadence: bool,
}

fn strip(conf: StripConf) -> impl Fn(tcx::TrackPoint) -> tcx::TrackPoint {
    let f = move |mut point: tcx::TrackPoint| -> tcx::TrackPoint {
        if conf.watts {
            point.extensions = None;
        }
        if conf.cadence {
            point.cadence = None;
        }
        return point;
    };

    return f
}

fn strip_streams(n: usize, points: Vec<tcx::TrackPoint>) -> Vec<tcx::TrackPoint> {
    return points
        .into_iter()
        .map(strip(match n {
            0 => StripConf{watts: true, ..Default::default()},
            _ => StripConf{cadence: true, ..Default::default()}
        }))
        .collect();
}

fn main() {
    env_logger::init();
    let args: Vec<String> = env::args().collect();

    // HACK: parsing the files *twice*, just to avoid the borrow checker!!
    let acts: Vec<tcx::Activity> = args[1..]
        .iter()
        .map(parse)
        .flat_map(|x| x)
        .flat_map(|db| db.activities.activity)
        .collect();

    let merged = aggregate(acts);

    let mut points: Vec<tcx::TrackPoint> = args[1..]
        .iter()
        .map(parse)
        .flat_map(|x| x)
        .flat_map(|db| db.activities.activity)
        .enumerate()
        .flat_map(|pair| strip_streams(pair.0, pair.1.lap.track.trackpoint))
        .collect();

    // TODO, eventually -- allow configuration of which streams are kept for each db before
    // collecting
    points.sort_by(|a, b| a.time.cmp(&b.time));

    // NOTE: for now, only support merging everything into a single activity and lap
    let mut out = tcx::TrainingCenterDatabase{
        activities: tcx::Activities{
            activity: vec![tcx::Activity{
                id: merged.start_time.clone(),
                lap: tcx::Lap{
                    // TODO: all these fields need to be aggregated across all dbs
                    start_time: merged.start_time,
                    total_time_seconds: merged.total_time,
                    distance_meters: merged.distance_meters,
                    maximum_speed: merged.maximum_speed,
                    calories: merged.calories, 
                    average_heart_rate_bpm: None,
                    maximum_heart_rate_bpm: None,
                    cadence: merged.cadence,
                    intensity: merged.intensity,
                    trigger_method: merged.trigger,
                    track: tcx::Track{
                        trackpoint: points
                    }
                },
                sport: "Biking".to_string()
            }
            ]
        } 
    };

    // TODO: allow user to specify output file
    let file = fs::File::create("out.tcx").expect(&format!("Cannot open file {}", "out.tcx"));
    let mut writer = BufWriter::new(file);
    tcx::write(&mut writer, &mut out);
}
